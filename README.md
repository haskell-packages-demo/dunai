# dunai

S[R](https://repology.org/project/haskell:dunai/versions)Ni Generalised reactive framework supporting classic, arrowized and monadic FRP

* [*Implementation of Monadic Stream Functions supporting Classic and Arrowized Functional Reactive Programming, and Stream programming*
  ](https://laptrinhx.com/implementation-of-monadic-stream-functions-supporting-classic-and-arrowized-functional-reactive-programming-and-stream-programming-1262644698/)
  2020-06-04 ivanperez-keera
